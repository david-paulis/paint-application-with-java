/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPaint;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author david
 */
public class Paint extends Applet {

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    ArrayList<MyShape> shapes = new ArrayList<MyShape>();

    ArrayList<MyRect> rectBin = new ArrayList<MyRect>();

    MyRect rect;
    Color color = Color.decode("#000000"); //#000000 black ,#FF0000 red , #0000FF blue , #008000 green 
    int shapeType = 0; // 0 line , 1 ovar , 2 rect;
    int x1, y1, x2, y2, width = 0, height = 0;
    boolean fill = false;

    private void restoreDefault() {
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        width = 0;
        height = 0;

    }

    public void init() {
        // TODO start asynchronous download of heavy resources
        Button blackbtn = new Button("BLACK");
        Button redbtn = new Button("RED");
        Button greenbtn = new Button("GREEN");
        Button bluebtn = new Button("BLUE");
        Button linebtn = new Button("LINE");
        Button ovalbtn = new Button("OVAL");
        Button rectbtn = new Button("RECT");
        Checkbox ftbtn = new Checkbox("FILL");
        Button clrAllbtn = new Button("Clear All");
        Button undobtn = new Button("Undo");
        Button savebtn = new Button("save");
        Button openbtn = new Button("open");

        add(blackbtn);
        add(redbtn);
        add(greenbtn);
        add(bluebtn);
        add(linebtn);
        add(ovalbtn);
        add(rectbtn);

        add(ftbtn);
        add(clrAllbtn);
        add(undobtn);
        add(savebtn);
        add(openbtn);
        openbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("shapes"));
                    shapes.clear();
                    while (true) {
                        
                        shapes.add((MyShape) ois.readObject());
                    }
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                } catch(EOFException ex){
                  repaint();
                }
                
                catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    repaint();
                }
            }
        });
        savebtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ObjectOutputStream objectOut = null;
                try {
                    // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    objectOut = new ObjectOutputStream(new FileOutputStream("shapes"));
                    for (MyShape shape : shapes) {
                        objectOut.writeObject(shape);

                    }
                    objectOut.close();

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });

        undobtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                try {
                    shapes.remove(shapes.size() - 1);
                    repaint();
                } catch (ArrayIndexOutOfBoundsException ex) {

                }
            }

        });

      
        clrAllbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                shapes.clear();
                rectBin.clear();
                repaint();

            }
        });
       

       
        ftbtn.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                 if (fill == true) {
                    fill = false;
                } else {
                    fill = true;
                }
            }
        });
        blackbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                color = Color.decode("#000000"); //#000000 black ,#FF0000 red , #0000FF blue , #008000 green 
            }
        });
        bluebtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                color = Color.decode("#0000FF"); //#000000 black ,#FF0000 red , #0000FF blue , #008000 green 

            }
        });
        redbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                color = Color.decode("#FF0000"); //#000000 black ,#FF0000 red , #0000FF blue , #008000 green 

            }
        });
        greenbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                color = Color.decode("#008000"); //#000000 black ,#FF0000 red , #0000FF blue , #008000 green 

            }
        });
        linebtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                shapeType = 0;

                restoreDefault();
            }
        });
        ovalbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.
                shapeType = 1;

                restoreDefault();
            }
        });
        rectbtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //To change body of generated methods, choose Tools | Templates.

                shapeType = 2;
                restoreDefault();
            }
        });

        Draw lineListener = new Draw();

        addMouseListener(lineListener);
        addMouseMotionListener(lineListener);

    }

    class Draw extends MouseInputAdapter {

        public void mousePressed(MouseEvent e) {

            x1 = e.getX();
            y1 = e.getY();
          

        }

        public void mouseDragged(MouseEvent e) {
            if (shapeType == 0) {
                x2 = e.getX();
                y2 = e.getY();

            } else if (shapeType == 1 || shapeType == 2) {
                x2 = e.getX();
                y2 = e.getY();
                if (y1 < y2) {
                    height++;
                } else {
                    height--;
                }
                if (x1 < x2) {
                    width++;
                } else {
                    width--;
                }
            } 

            repaint();
        }

        public void mouseReleased(MouseEvent e) {
            if (shapeType == 0) {
                x2 = e.getX();
                y2 = e.getY();
                MyLine line = new MyLine();
                line.setColor(color);
                line.setX(x1);
                line.setY(y1);
                line.setX2(x2);
                line.setY2(y2);
                line.setFill(fill);
                shapes.add(line);
            } else if (shapeType == 1) {
                MyOval oval = new MyOval();
                oval.setColor(color);
                oval.setFill(fill);
                oval.setX(x1);
                oval.setY(y1);
                oval.setWidth(width);
                oval.setHeight(height);
                shapes.add(oval);
            } else if (shapeType == 2) {
                MyRect rect = new MyRect();
                rect.setX(x1);
                rect.setY(y1);
                rect.setWidth(width);
                rect.setHeight(height);
                rect.setColor(color);
                rect.setFill(fill);
                shapes.add(rect);
            } 
            repaint();
            restoreDefault();
        }

    }

    // TODO overwrite start(), stop() and destroy() methods
    @Override
    public void paint(Graphics g) {
        for (MyShape shape : shapes) {
            if (shape.getClass().getSimpleName().equals("MyLine")) {

                MyLine line = (MyLine) shape;
                g.setColor(line.getColor());
                g.drawLine(line.getX(), line.getY(), line.getX2(), line.getY2());
            } else if (shape.getClass().getSimpleName().equals("MyOval")) {
                MyOval oval = (MyOval) shape;
                g.setColor(oval.getColor());
                if (oval.isFill()) {
                    g.fillOval(oval.getX(), oval.getY(), oval.getWidth(), oval.getHeight());
                }

                g.drawOval(oval.getX(), oval.getY(), oval.getWidth(), oval.getHeight());

            } else if (shape.getClass().getSimpleName().equals("MyRect")) {
                MyRect rect = (MyRect) shape;
                g.setColor(rect.getColor());
                if (rect.isFill()) {
                    g.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
                }

                g.drawRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
            }
        }
       
        if (shapeType == 0) {
            g.setColor(color);
            g.drawLine(x1, y1, x2, y2);
        } else if (shapeType == 1) {
            g.setColor(color);
            g.drawOval(x1, y1, width, height);
        } else if (shapeType == 2) {
            g.setColor(color);
            g.drawRect(x1, y1, width, height);
        }

     
    }
}
